const path = require('path');

const config = {
  entry: './src/index.js',
  mode: 'production',
  output: {
    filename: 'main.js',
    path: path.resolve(__dirname, 'public/dist'),
  },
};

module.exports = config;
