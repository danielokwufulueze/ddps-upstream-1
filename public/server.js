const express = require('express');
const path = require('path');

const app = express();
const port = process.env.PORT || 5353;

app.get('/', function(req, res) {
  res.sendFile(path.join(__dirname, '/index.html'));
});

app.get('/dist/main.js', function(req, res) {
  res.sendFile(path.join(__dirname, 'dist/main.js'));
});

app.listen(port);

console.log(':::Server running at http://localhost:' + port);
