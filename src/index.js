async function greet() {
  const url = 'http://localhost:5049/hello';
  const response = await fetch(url);
  const greeting = await (response.text());
  const element = document.createElement('div');

  element.innerHTML = greeting;
  document.body.appendChild(element);
}

greet();
